
def test1(arr1, arr2):
    # n : size of arr1
    # m : size of arr2
    res = []
    s1 = set(arr1) # O(n)
    for elem in arr2: # O(m)
        if elem in s1: # O(1)
            res.append(elem) # O(1)
    return res # Result is O(n+m)
    # Код можно упростить кастанув оба массива в сет
    # и потом вызвав intersection, но асимптотика не поменяется
    # а второс список ввсе равно прийдется итерировать


def test2(arr):
    i = len(arr) - 1
    while i >= 0:
        if arr[i] == 0:
            arr.pop(i)
        i -= 1
    # За счет того что идем с конца к началу
    # при удалении двигаются не все элементы, а лишь ненулевые


if __name__ == "__main__":

    print(test1([1,2,3,4,5,6,7], [8,9,0, 1,5]))

    arr = [0, 1, 0, 0, 4, 5, 6, 7, 8, -4, 0]
    test2(arr)
    print(arr)
